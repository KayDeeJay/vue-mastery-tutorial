Vue.component('product-details', {
  props: {
    details: {
      type: Array,
      required: true
    }
  },
  template: `
    <ul>
      <li v-for="detail in details">{{ detail }}</li>
    </ul>
  `
})

Vue.component('product', {
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  template: `
  <div class="product">
    <div class="product-image">
      <img :src="image">
    </div>
    <div class="product-info">
      <h1>{{ title }}</h1>
      <h2><a target="blank" :href="link">link to my blog</a></h2>
      <p v-if="inStock">In Stock!</p>
      <p v-else :class="{ 'out-of-stock': !inStock }">Out of Stock!</p>
      <p>{{ sale }}</p>
      <p>Shipping: {{ shipping }}</p>
      <product-details :details="details" />
      <ul>
        <li v-for="size in sizes">{{ size }}</li>
      </ul>
      <div v-for="(variant, index) in variants"
        :key="variant.variantId"
        class="color-box"
        :style="{backgroundColor: variant.variantColor }"
        @mouseover="updateProduct(index)" >
      </div>
      <button @click="addToCart"
              :disabled="!inStock"
              :class="{ disabledButton: !inStock }">Add to Cart</button>
      <div class="cart">
        <p>Cart ({{ cart }})</p>
      </div>
      <button @click="removeFromCart">Remove from Cart</button>
    </div>
  </div>
  `,
  data() {
    return {
      brand: 'Vue Mastery',
      product: 'Socks',
      cart: 0,
      selectedVariant: 0,
      link: 'https://kevincollectshobbies.pythonanywhere.com',
      onSale: true,
      details: ['80% Cotton','20% Polyester','Gender-Neutral'],
      sizes: ['Small','Medium','Large'],
      variants: [
        {
          variantId: 2234,
          variantColor: 'Green',
          variantImage:'./Assets/GreenSocks.jpeg',
          variantQuantity: 10
        },
        {
          variantId: 2235,
          variantColor: 'Blue',
          variantImage:'./Assets/BlueSocks.jpeg',
          variantQuantity: 0
        }
      ],
    }
  },
  methods: {
    addToCart(){
      this.cart += 1;
    },
    updateProduct(index){
      this.selectedVariant = index;
    },
    removeFromCart(){
      if (this.cart > 0) {
        this.cart -= 1;  
      }
    }
  },
  computed: {
    title(){
      return this.brand + ' ' + this.product;
    },
    image(){
      return this.variants[this.selectedVariant].variantImage;
    },
    inStock(){
      return this.variants[this.selectedVariant].variantQuantity;
    },
    sale(){
      if (this.onSale) {
        return(this.brand + ' ' + this.product + ' are On Sale!');
      }
      else {
        return(this.brand + ' ' + this.product + ' are Sold Out!');
      }
    },
    shipping(){
      if (this.premium) {
        return 'free';
      }
      return '$2.99';
    }
  }
})

const app = new Vue({
  el: '#app',
  data: {
    premium: false
  }
});
